FROM docker.io/bitnami/mongodb:4.2.3-debian-10-r10
COPY config/run.sh /run.sh
USER root
CMD [ "/run.sh" ]
